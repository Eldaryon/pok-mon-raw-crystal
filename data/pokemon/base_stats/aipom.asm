	db AIPOM ; 190

	db  65,  60,  75,  105,  100,  75
	;   hp  atk  def  spd  sat  sdf

	db FAIRY, PSYCHIC ; type
	db 45 ; catch rate
	db 94 ; base exp
	db NO_ITEM, NO_ITEM ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 20 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/aipom/front.dimensions"
	db 0, 0, 0, 0 ; padding
	db GROWTH_FAST ; growth rate
	dn EGG_FAIRY, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HEADBUTT, CURSE, TOXIC, ZAP_CANNON, HIDDEN_POWER, SUNNY_DAY, SNORE, PROTECT, ENDURE, FRUSTRATION, IRON_TAIL, THUNDER, RETURN, SHADOW_BALL, MUD_SLAP, DOUBLE_TEAM, BLIZZARD, SWAGGER, SLEEP_TALK, SWIFT, PSYCHIC_M, SOLARBEAM, DREAM_EATER, DETECT, REST, ATTRACT, THIEF,  NIGHTMARE, THUNDERBOLT, SURF
	; end
