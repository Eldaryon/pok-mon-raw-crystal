	db COMBUSTARI ; 252

	db  65,  75,  57,  110, 130,  58
	;   hp  atk  def  spd  sat  sdf

	db FIRE, WATER ; type
	db 40 ; catch rate
	db 165 ; base exp
	db BURNT_BERRY, CHARCOAL ; items
	db GENDER_F100 ; gender ratio
	db 100 ; unknown 1
	db 25 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/combustari/front.dimensions"
	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate
	dn EGG_WATER_1, EGG_FAIRY ; egg groups

	; tm/hm learnset
	tmhm HEADBUTT, CURSE, TOXIC, HIDDEN_POWER, SUNNY_DAY, SNORE, HYPER_BEAM, PROTECT, ENDURE, FRUSTRATION, IRON_TAIL, RETURN, PSYCHIC_M, MUD_SLAP, SWAGGER, SLEEP_TALK, FIRE_BLAST, DETECT, REST, ATTRACT, THIEF, FLAMETHROWER, SURF, WHIRLPOOL, WATERFALL
	; end
	