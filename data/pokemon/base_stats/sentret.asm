	db SENTRET ; 161

	db  45,  80,  50,  60,  65,  30
	;   hp  atk  def  spd  sat  sdf

	db DARK, FAIRY ; type
	db 182 ; catch rate
	db 57 ; base exp
	db BERRY, MOON_STONE ; items
	db GENDER_F50 ; gender ratio
	db 100 ; unknown 1
	db 15 ; step cycles to hatch
	db 5 ; unknown 2
	INCBIN "gfx/pokemon/sentret/front.dimensions"
	db 0, 0, 0, 0 ; padding
	db GROWTH_MEDIUM_FAST ; growth rate
	dn EGG_FAIRY, EGG_GROUND ; egg groups

	; tm/hm learnset
	tmhm HEADBUTT, CURSE, TOXIC, HIDDEN_POWER, RAIN_DANCE, SNORE, PROTECT, ENDURE, FRUSTRATION, IRON_TAIL, RETURN, DIG, SHADOW_BALL, MUD_SLAP, DOUBLE_TEAM, ICE_PUNCH, SWAGGER, SLEEP_TALK, SWIFT, DEFENSE_CURL, THUNDERPUNCH, DETECT, REST, ATTRACT, THIEF, FURY_CUTTER, CUT, SURF, WATERFALL, FLASH, ICY_WIND, BLIZZARD, SOLARBEAM, GIGA_DRAIN
	; end
