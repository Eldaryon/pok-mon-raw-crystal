	db "NIGHT CAT@" ; species name
	dw 511, 720 ; height, weight

	db   "They harness the"
	next "power of the moon"
	next "to empower their "

	page "physical abilities."
	next "They appear at"
	next "night to hunt.@"
