	db "MINIKAT@" ; species name
	dw 207, 250 ; height, weight

	db   "The smell of the "
	next "clouds on its head"
	next "can appease the "

	page "wildest #MON."
	next "It has acute PSY"
	next "and FAIRY powers.@"
