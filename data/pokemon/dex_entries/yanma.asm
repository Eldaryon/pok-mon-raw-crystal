	db "SHARK@" ; species name
	dw 311, 840 ; height, weight

	db   "It gnaws on boats"
	next "to harden its"
	next "teeth. It is not"

	page "really harmful"
	next "though, and loves"
	next "to make friends.@"
