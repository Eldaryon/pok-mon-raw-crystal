	db "ANCHOR@" ; species name
	dw 104, 190 ; height, weight

	db   "Its anchor is very"
	next "sharp. It can cut"
	next "through anything."

	page "Its teeth are also"
	next "very sturdy as it"
	next "has sharpened them@"
