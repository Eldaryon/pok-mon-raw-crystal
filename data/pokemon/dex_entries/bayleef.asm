	db "FLOWER@" ; species name
	dw 311, 350 ; height, weight

	db   "It has taken a "
	next "temporary form "
	next "before it fully "

	page "evolves. Its smell"
	next "is said to make"
	next "people energetic.@"
