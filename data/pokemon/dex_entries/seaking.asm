	db "SWIMMER@" ; species name
	dw 403, 860 ; height, weight

	db   "They are extremely"
	next "proud of their "
	next "newfound strength."

	page "They can move"
	next "without effort in"
	next "troubled water.@"
