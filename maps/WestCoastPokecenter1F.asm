	const_def 2 ; object constants
	const WESTCOASTPOKECENTER1F_NURSE
	const WESTCOASTGENTLEMAN
	const WESTCOASTE

WestCoastPokecenter1F_MapScripts:
	db 1 ; scene scripts
	scene_script .DummyScene

	db 0 ; callbacks

.DummyScene:
	end

WestCoastPokecenter1FNurseScript:
	jumpstd pokecenternurse

WestCoastPCGetleman:
	jumptextfaceplayer WCPCGentlemanScript

WestCoastE:
	jumptextfaceplayer EText

WCPCGentlemanScript:
	text "Damn those Millen-"
	line "ials! Why should"
	cont "they have humour"
	cont "we don't get?"
	done

EText:
	text "E"
	done

WestCoastPokecenter1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  3,  7, WESTCOAST_CITY, 9
	warp_event  4,  7, WESTCOAST_CITY, 9
	warp_event  0,  7, POKECENTER_2F, 1

	db 0 ; coord events

	db 0 ; bg events

	db 3 ; object events
	object_event  3,  1, SPRITE_NURSE, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastPokecenter1FNurseScript, -1
	object_event  8,  4, SPRITE_GENTLEMAN, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastPCGetleman, -1
	object_event  1,  5, SPRITE_YOUNGSTER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastE, -1