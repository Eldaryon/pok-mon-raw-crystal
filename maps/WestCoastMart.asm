	const_def 2 ; object constants
	const WESTCOASTMART_CLERK
	const WESTCOAST

WestCoastMart_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestCoastMartClerkScript:
	opentext
	pokemart MARTTYPE_STANDARD, MART_VIOLET
	closetext
	end

WestCoastYoungsterScript:
	jumptextfaceplayer WCMartYoungster

WCMartYoungster:
	text "I really like to"
	line "window shop here."

	para "They have such a"
	line "colorful array of"
	cont "#MON products!"
	done

WestCoastMart_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  2,  7, WESTCOAST_CITY, 8
	warp_event  3,  7, WESTCOAST_CITY, 8

	db 0 ; coord events

	db 0 ; bg events

	db 2 ; object events
	object_event  1,  3, SPRITE_CLERK, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastMartClerkScript, -1
	object_event  6,  6, SPRITE_YOUNGSTER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 0, 0, -1, -1, PAL_NPC_BROWN, OBJECTTYPE_SCRIPT, 0, WestCoastYoungsterScript, -1
