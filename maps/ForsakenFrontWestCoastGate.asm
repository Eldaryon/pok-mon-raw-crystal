	const_def 2 ; object constants

ForsakenFrontWestCoastGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

ForsakenFrontWestCoastGate_MapEvents:
	db 0, 0 ; filler

	db 4 ; warp events
	warp_event  4,  0, FORSAKEN_FRONT, 1
	warp_event  5,  0, FORSAKEN_FRONT, 2
	warp_event  4,  7, WESTCOAST_CITY, 11
	warp_event  5,  7, WESTCOAST_CITY, 11

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
