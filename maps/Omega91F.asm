	const_def 2 ; object constants
	const Omega91F_RECEPTIONIST
	const Omega91F_OFFICER

Omega91F_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Omega9ReceptionistScript:
	jumptextfaceplayer Omega9ReceptionistText

Omega9OfficerScript:
	jumptextfaceplayer Omega9OfficerText

Omega9ReceptionistText:
	text "Welcome. This is"
	line "Omega9's"
	cont "Headquarters."

	para "We are testing an"
	line "undisclosed"
	cont "project, so you"

	para "may not be able"
	line "to access upper"
	cont "floors of the HQ."
	done

Omega9OfficerText:
	text "Only employees are"
	line "permitted to go"
	cont "upstairs."

	para "Sorry champ."
	done

Omega91F_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  2,  7, WESTCOAST_CITY, 3
	warp_event  3,  7, WESTCOAST_CITY, 4

	db 0 ; coord events

	db 0 ; bg events

	db 2 ; object events
	object_event  4,  2, SPRITE_RECEPTIONIST, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, PAL_NPC_RED, OBJECTTYPE_SCRIPT, 0, Omega9ReceptionistScript, -1
	object_event 13,  1, SPRITE_OFFICER, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, PAL_NPC_RED, OBJECTTYPE_SCRIPT, 0, Omega9OfficerScript, -1
