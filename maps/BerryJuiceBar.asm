	const_def 2 ; object constants
	const BERRYBAR_SUPER_NERD
	const BERRYBAR_FISHER1
	const BERRYBAR_FISHER2
	const BERRYBAR_FISHER3

BerryJuiceBar_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

BerryJuiceBarChefBR:
	faceplayer
	opentext
	writetext ChefText_EatathonBR
	waitbutton
	closetext
	end

BerryJuiceBarFisher1:
	opentext
	writetext Fisher1Text_SnarfleWestCoast
	waitbutton
	closetext
	faceplayer
	opentext
	writetext Fisher1Text_ConcentrationWestCoast
	waitbutton
	closetext
	turnobject BERRYBAR_FISHER1, LEFT
	end

BerryJuiceBarFisher2:
	faceplayer
	opentext
	writetext BetsyText
	waitbutton
	closetext
	turnobject BERRYBAR_FISHER2, RIGHT
	end

BerryJuiceBarFisher3:
	opentext
	writetext Fisher3Text_MunchMunchWestCoast
	waitbutton
	closetext
	faceplayer
	opentext
	writetext Fisher3Text_CeladonIsBest
	waitbutton
	closetext
	turnobject BERRYBAR_FISHER3, RIGHT
	end
EatathonBRContestPoster:
	jumptext EatathonBRContestPosterText

BerryJuiceBarTrashcan:
	checkevent EVENT_FOUND_LEFTOVERS_IN_CELADON_CAFE
	iftrue .TrashEmpty
	giveitem LEFTOVERS
	iffalse .PackFull
	opentext
	itemtotext LEFTOVERS, MEM_BUFFER_0
	writetext FoundLeftoversTextWestCoast
	playsound SFX_ITEM
	waitsfx
	itemnotify
	closetext
	setevent EVENT_FOUND_LEFTOVERS_IN_CELADON_CAFE
	end

.PackFull:
	opentext
	itemtotext LEFTOVERS, MEM_BUFFER_0
	writetext FoundLeftoversTextWestCoast
	buttonsound
	writetext NoRoomForLeftoversTextWestCoast
	waitbutton
	closetext
	end

.TrashEmpty:
	jumpstd trashcan

ChefText_EatathonBR:
	text "Hi!"

	para "We're holding the"
	line "Eatathon Battle"
	cont "Royale."

	para "We can't serve you"
	line "right now. Sorry."
	done

Fisher1Text_SnarfleWestCoast:
	text "…Snarfle, chew…"
	done

Fisher1Text_ConcentrationWestCoast:
	text "Don't talk to me!"

	para "I'm gonna build"
	line "a Bone Tower!"
	done

BetsyText:
	text "..."
	line "I'm busy."

	para "Go play somewhere"
	line "else."
	done

Fisher3Text_MunchMunchWestCoast:
	text "Munch, munch…"
	done

Fisher3Text_CeladonIsBest:
	text "I hope I can get"
	line "a good food drop"
	cont "next."

	para "I'm getting some"
	line "pretty hard to"
	cont "digest food."
	done

EatathonBRContestPosterText:
	text "Eatathon Battle"
	line "Royale!"

	para "100 Gluttons, only"
	line "one victor! The"

	para "muncher gets it"
	line "all for free!"
	done

FoundLeftoversTextWestCoast:
	text "<PLAYER> found"
	line "@"
	text_from_ram wStringBuffer3
	text "!"
	done

NoRoomForLeftoversTextWestCoast:
	text "But <PLAYER> can't"
	line "hoard more trash."
	done

BerryJuiceBar_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  6,  7, WESTCOAST_CITY, 5
	warp_event  7,  7, WESTCOAST_CITY, 5

	db 0 ; coord events

	db 2 ; bg events
	bg_event  5,  0, BGEVENT_READ, EatathonBRContestPoster
	bg_event  7,  1, BGEVENT_READ, BerryJuiceBarTrashcan

	db 4 ; object events
	object_event  9,  3, SPRITE_FISHER, SPRITEMOVEDATA_STANDING_LEFT, 0, 0, -1, -1, PAL_NPC_BROWN, OBJECTTYPE_SCRIPT, 0, BerryJuiceBarChefBR, -1
	object_event  4,  6, SPRITE_FISHER, SPRITEMOVEDATA_STANDING_LEFT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, BerryJuiceBarFisher1, -1
	object_event  7,  4, SPRITE_BUGSY, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, PAL_NPC_RED, OBJECTTYPE_SCRIPT, 0, BerryJuiceBarFisher2, -1
	object_event  1,  2, SPRITE_FISHER, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, BerryJuiceBarFisher3, -1
