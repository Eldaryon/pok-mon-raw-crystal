	const_def 2 ; object constants
	const WESTCOASTGYM_GUY

WestCoastGym_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestCoastGymGuy:
	jumptextfaceplayer WhyIsTheGymEmpty

WhyIsTheGymEmpty:
	text "You're here for the"
	line "Gym challenge?"

	para "Sorry, but I'm"
	line "gonna disappoint."

	para "This Gym has"
	line "closed when we"
	cont "became affiliated"
	cont "to the KANTO"
	cont "league."

	para "If you're looking"
	line "for the Leader..."

	para "He spends most of"
	line "his time at the"
	cont "Berry Juice bar."
	done

WestCoastGym_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  6,  9, WESTCOAST_CITY, 7
	warp_event  7,  9, WESTCOAST_CITY, 7

	db 0 ; coord events

	db 0 ; bg events

	db 1 ; object events
	object_event 6,  2, SPRITE_COOLTRAINER_M, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, WestCoastGymGuy, -1