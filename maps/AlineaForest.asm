	const_def 2 ; object constants
	const ALINEA_TEACHER
	const ALINEA_BUG_CATCHER
	const ALINEA_POKE_BALL
	const ALINEA_MEWN

AlineaForest_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_OBJECTS, .Mewn

.Mewn:
	checktime NITE
	iftrue .Appear
	iffalse .NoAppear
.Appear:
	appear ALINEA_MEWN
	return
.NoAppear:
	disappear ALINEA_MEWN
	return

AlineaMewn:
	faceplayer
	cry SENTRET
	loadwildmon SENTRET, 5
	writecode VAR_BATTLETYPE, BATTLETYPE_SHINY
	startbattle
	disappear ALINEA_MEWN
	setflag EVENT_SAW_MEWN
	reloadmapafterbattle
	end

AlineaBugCatcherScript:
	jumptextfaceplayer AlineaBugCatcherText

AlineaTeacherScript:
	jumptextfaceplayer AlineaTeacherText

AlineaItemBall:
 	itemball POTION

AlineaBugCatcherText:
 	text "I really like"
 	line "BUTTERFREE."

 	para "But it's so bad"
 	line "Smogon says it's"

 	para "the most useless"
 	line "#MON."

 	para "That's why I'm"
 	line "waiting for better"

 	para "BUG types to be"
 	line "added to the game."
 	done

AlineaTeacherText:
 	text "I heard there is a"
 	line "very rare #MON"

 	para "that lives here"
 	line "called MEWN."

 	para "It must be sooo"
 	line "cute looking!"
 	done

AlineaForest_MapEvents:
	db 0, 0 ; filler

	db 4 ; warp events
	warp_event  41, 24, ROUTE_29, 2
	warp_event  41, 25, ROUTE_29, 3
	warp_event   2, 10, ROUTE_29, 4
	warp_event   2, 11, ROUTE_29, 5

	db 0 ; coord events

	db 0 ; bg events

	db 4 ; object events
	object_event 23, 19, SPRITE_BUG_CATCHER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, AlineaBugCatcherScript, -1
	object_event 25, 14, SPRITE_TEACHER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, AlineaTeacherScript, -1
	object_event 2,  17, SPRITE_POKE_BALL, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_ITEMBALL, 0, AlineaItemBall, EVENT_ALINEA_POTION
	object_event 21,  4, SPRITE_MONSTER, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, AlineaMewn, EVENT_SAW_MEWN