	const_def 2 ; object constants

Route2FWestCoastGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route2FWestCoastGate_MapEvents:
	db 0, 0 ; filler

	db 4 ; warp events
	warp_event  9, 4, ROUTE_2F, 1
	warp_event  9, 5, ROUTE_2F, 2
	warp_event  0, 4, WESTCOAST_CITY, 1
	warp_event  0, 5, WESTCOAST_CITY, 2	

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
	