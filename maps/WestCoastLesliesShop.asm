	const_def 2 ; object constants
	const LESLIESSHOP_CLERK
	const LESLIESSHOP_ROCKET

WestCoastLesliesShop_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

WestCoastLesliesShopClerkScript:
	faceplayer
	opentext
	pokemart MARTTYPE_STANDARD, MART_ROCKSTORE
	closetext
	end

WestCoastLesliesShopRocketScript:
	jumptextfaceplayer RocketLeslie

RocketLeslie:
	text "Leslie comes from"
	line "a faraway land"
	cont "called ALOLA."

	para "Some #MON there"
	line "are very different"
	cont "from here!"
	done

WestCoastLesliesShop_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  3,  7, WESTCOAST_CITY, 10
	warp_event  4,  7, WESTCOAST_CITY, 10


	db 0 ; coord events

	db 0 ; bg events

	db 2 ; object events
	object_event 1,  2, SPRITE_GRANNY, SPRITEMOVEDATA_STANDING_RIGHT, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastLesliesShopClerkScript, -1
	object_event 1,  6, SPRITE_ROCKET, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastLesliesShopRocketScript, -1
