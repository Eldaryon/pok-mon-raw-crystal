	const_def 2 ; object constants
	const WESTCOAST_ELDARYON
	const WESTCOAST_ROCKET
	const WESTCOAST_CLEFAIRY
	const WESTCOAST_LASS
	const WESTCOAST_OLDMAN

WestCoastCity_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .Flypoint

.Flypoint:
	setflag ENGINE_FLYPOINT_WESTCOAST
	return


WestCoastClefairyScript:
	faceplayer
	cry CLEFAIRY
	opentext
	writetext ClefairyText
	waitbutton
	closetext
	end

WestCoastEldaryonScript:
	jumptextfaceplayer EldaryonStopsYou

WestCoastRocketScript:
	jumptextfaceplayer RocketStopsYou

WestCoastLassScript:
	jumptextfaceplayer LassText

WestCoastGrampsScript:
	jumptextfaceplayer GrampsText

WestCoastSign:
	jumptext WestCoastSignText

WestCoastCenterSign:
	jumpstd pokecentersign

WestCoastShopSign:
	jumpstd martsign

WestCoastEldaryonHouseSign:
	jumptext EldaryonHouseSignText

WestCoastOmega9Sign:
	jumptext Omega9SignText

WestCoastBerryJuiceBarSign:
	jumptext BerryJuiceBarText

WestCoastJewelryShopSign:
	jumptext JewelryShopText

WestCoastGymSign:
	jumptext GymSign

EldaryonStopsYou:
	text "Puff... Puff..."
	line "Uh... I made it in"
	cont "time..."

	para "Sorry <PLAYER>."
	line "No pass for now."
	done

RocketStopsYou:
	text "Sorry, but our"
	line "shop is still not"
	cont "open."

	para "..."
	line "What?"

	para "Never saw someone"
	line "wearing black,"
	cont "gothic clothes?"

	para "What do you mean"
	line "I look like a "
	cont "criminal?"
	done

WestCoastSignText:
	text "Westcoast City"

	para "The City of Heroes"
	done

EldaryonHouseSignText:
	text "Eldaryon's House"

	para "The sign seems"
	line "pretty new."
	done

Omega9SignText:
	text "    Omega9  HQ    "

	para "We bring happiness"
	line "to your doorstep."
	done

BerryJuiceBarText:
	text "  -Berry Juice-"
	line "  Bar/Restaurant"
	done

GymSign:
	text "Westcoast Gym"

	para "It is crossed"
	line "with red paint..."
	done

JewelryShopText:
	text "Leslie's Jewelry"
	line "      Shop"

	para "All customers"
	line "welcome!"
	done

LassText:
	text "I just love FAIRY"
	line "types!"

	para "They counter "
	line "DRAGON types with"
	cont "zaz!"
	done

GrampsText:
	text "I am an avid fan"
	line "of #MON science"
	cont "facts."

	para "My favourite"
	line "subject is"
	cont "evolution."

	para "I still remember"
	line "reading a paper"
	cont "made by Dr. Fuji"

	para "that speaks about"
	line "what's beyond it."
	done

ClefairyText:
	text "Clef Clef!"
	done

WestCoastCity_MapEvents:
	db 0, 0 ; filler

	db 11 ; warp events
	warp_event  27, 16, ROUTE2F_WESTCOAST_GATE, 3
	warp_event  27, 17, ROUTE2F_WESTCOAST_GATE, 4
	warp_event  20,  5, OMEGA91F, 1
	warp_event  22,  5, OMEGA91F, 2
	warp_event  23, 21, BERRY_JUICE_BAR, 1
	warp_event  17, 21, WESTCOAST_ELDARYONS_HOUSE, 1
	warp_event   6, 21, WESTCOAST_GYM, 1
	warp_event   5,  5, WESTCOAST_MART, 1
	warp_event  15, 13, WESTCOAST_POKECENTER_1F, 1
	warp_event   7, 11, LESLIE_ROCK_STORE, 1
	warp_event  11,  3, FORSAKEN_FRONT_WEST_COAST_GATE, 3

	db 0 ; coord events

	db 8 ; bg events
	bg_event 22, 10, BGEVENT_READ, WestCoastSign
	bg_event  6,  5, BGEVENT_READ, WestCoastShopSign
	bg_event 16, 13, BGEVENT_READ, WestCoastCenterSign
	bg_event 20, 21, BGEVENT_READ, WestCoastEldaryonHouseSign
	bg_event 18,  7, BGEVENT_READ, WestCoastOmega9Sign
	bg_event 23, 23, BGEVENT_READ, WestCoastBerryJuiceBarSign
	bg_event  9, 13, BGEVENT_READ, WestCoastJewelryShopSign
	bg_event  4, 23, BGEVENT_READ, WestCoastGymSign

	db 5 ; object events
	object_event 11,  4, SPRITE_COOLTRAINER_M, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, WestCoastEldaryonScript, EVENT_GOT_TM45_ATTRACT
	object_event  7, 12, SPRITE_ROCKET, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastRocketScript, EVENT_RIVAL_BURNED_TOWER
	object_event 19, 14, SPRITE_FAIRY, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastClefairyScript, -1
	object_event 20, 14, SPRITE_LASS, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastLassScript, -1
	object_event  4,  7, SPRITE_GRAMPS, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, WestCoastGrampsScript, -1
	