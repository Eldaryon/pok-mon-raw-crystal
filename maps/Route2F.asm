	const_def 2 ; object constants
	const ROUTE2F_FISHER
	const ROUTE2F_ELDARYON
	const ROUTE2F_TREE
	const ROUTE2F_SUPER_NERD
	const ROUTE2F_YOUNGSTER1
	const ROUTE2F_YOUNGSTER2
	const ROUTE2F_SHIRENAI
	const ROUTE2F_FANGUTE

Route2F_MapScripts:
	db 0 ; scene scripts
	;scene_script .DummyScene0 ;
	;scene_script .DummyScene1 ;

;.DummyScene0:
	;end

;.DummyScene1:
	;end

	db 1 ; callbacks
	callback MAPCALLBACK_OBJECTS, .Shirenai

.Shirenai:
	checkflag EVENT_GAVE_MYSTERY_EGG_TO_ELM
	iffalse .NoAppearShirenai
	checkflag EVENT_ROUTE2F_UR_MOM_GAY
	iffalse .NoAppearShirenai

.NoAppearShirenai:
	;disappear ROUTE2F_SHIRENAI
	return

R2FShirenai:
	;moveobject ROUTE2F_SHIRENAI, 31, 5
	showemote EMOTE_SHOCK, ROUTE2F_SHIRENAI, 15
	special FadeOutMusic
	pause 15
	faceplayer
	;applymovement ROUTE2F_SHIRENAI, Route2F_ShirenaiWalksToYou
	playmusic MUSIC_ZINNIA_ENCOUNTER
	opentext
	writetext R2FShirenaiSpeaks
	waitbutton
	pause 15
	turnobject ROUTE2F_SHIRENAI, WEST
	writetext R2FShirenaiSpeaks2
	waitbutton
	turnobject ROUTE2F_SHIRENAI, DOWN
	writetext R2FShirenaiSpeaks3
	waitbutton
	closetext
	applymovement PLAYER, Route2F_YouWalkBack
	applymovement ROUTE2F_SHIRENAI, Route2F_ShirenaiWalksoff
	opentext
	writetext ShirenaiWarns
	waitbutton
	pause 15
	closetext
	applymovement ROUTE2F_SHIRENAI, Route2F_ShirenaiWalksoff2
	pause 15
	disappear ROUTE2F_SHIRENAI
	;setscene SCENE_R2F_NOTHING
	setflag EVENT_ROUTE2F_UR_MOM_GAY
	special RestartMapMusic
	end

R2FFangute:
	faceplayer
	cry YANMA
	loadwildmon YANMA, 19
	writecode VAR_BATTLETYPE, BATTLETYPE_SHINY
	startbattle
	disappear ROUTE2F_FANGUTE
	setflag EVENT_ROUTE2F_SAW_FANGUTE
	reloadmapafterbattle
	end

Route2FEldaryonScript:
	jumptextfaceplayer Route2FEldaryonText

Route2FFisherScript:
	jumptextfaceplayer Route2FFisherText

Route2FGameHouseSign:
	jumptext GameHouseSignText

Route2FTree:
	fruittree FRUITTREE_ROUTE_2F

Route2FSuperNerdScript:
	faceplayer
	opentext
	writetext R2FSuperNerdText
	waitbutton
	turnobject ROUTE2F_SUPER_NERD, UP
	closetext
	end

TrainerYoungsterAlex:
	trainer YOUNGSTER, ALEX2, EVEN_BEAT_YOUNGSTER_ALEX, AlexSeenText, AlexBeatenText, 0, .Script

.Script:
	endifjustbattled
	opentext
	writetext AlexAfterBattleText
	waitbutton
	closetext
	end

TrainerPsychicEtika:
	trainer PSYCHIC_T, ETIKA, EVEN_BEAT_ETIKA, EtikaSeenText, EtikaBeatenText, 0, .Script

.Script:
	endifjustbattled
	opentext
	writetext EtikaAfterBattleText
	waitbutton
	closetext
	end	

Route2F_ShirenaiWalksToYou:
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step DOWN
	step_end

Route2F_YouWalkBack:
	step DOWN
	step LEFT
	turn_head RIGHT
	step_end

Route2F_ShirenaiWalksoff:
	step DOWN
	step DOWN
	step RIGHT
	step RIGHT
	step_end

Route2F_ShirenaiWalksoff2:
	step RIGHT
	step RIGHT
	step RIGHT
	step RIGHT
	step_end

EtikaSeenText:
	text "You're <PLAYER>,"
	line "aren't you?"
	done

EtikaBeatenText:
	text "I didn't see this"
	line "one coming!"
	done

EtikaAfterBattleText:
	text "I met MEOWSY here"
	line "by day after I"
	cont "dreamed of it."

	para "He's so fluffy!"
	done

Route2FEldaryonText:
	text "Damn! I bought a"
	line "house on impulse"
	cont "again..."

	para "I wonder what can"
	line "I do with it."
	done

Route2FFisherText:
	text "The sea breeze"
	line "here is amazing."
	done

GameHouseSignText;
	text "Game House"

	para "Try you luck here!"
	line "and get some rare"
	cont "#MON!"
	done

R2FSuperNerdText:
	text "I came all the way"
	line "from HOENN to get"
	cont "these footprints!"

	para "Come back later!"
	done

AlexSeenText:
	text "I like Yoga pants!"
	line "They are long and"
	cont "comfy to wear!"
	done

AlexBeatenText:
	text "Damn! You got me."
	done

AlexAfterBattleText:
	text "As long as you're"
	line "wearing anything"
	cont "but shorts, I won't"
	cont "complain."
	done

R2FShirenaiSpeaks:
	text "Greetings."

	para "I am Shirenai."
	line "I originate from"
	cont "Snowlake Town."

	para "And you are?"

	para "<PLAYER>, you say?"

	para "You're still"
	line "a fledgeling tra-"
	cont "iner as I see."
	done

R2FShirenaiSpeaks2:
	text "If you keep going"
	line "west, you'll get"
	cont "to Westcoast City."

	para "Majority of trai-"
	line "ners avoid this"
	cont "part of Johto..."

	para "Especially after"
	line "the Gyms there "
	cont "stopped giving "
	cont "badges."

	para "The world is far"
	line "bigger than what"
	cont "those trainers"
	cont "think."

	para "They just choose"
	line "to ignore what"
	cont "they deem unfit"
	cont "and not lucrative"
	cont "to explore."
	done

R2FShirenaiSpeaks3:
	text "I am sorry..."

	para "Did I bore you?"

	para "I hope we can meet"
	line "again in the "
	cont "near future."

	para "You do seem to"
	line "to exude quite the"
	cont "aura."
	done

ShirenaiWarns:
	text "Oh and by the way."

	para "If I was you, I"
	line "would avoid Omega9"
	cont "at all costs."

	done

Route2F_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  2, 10, ROUTE2F_WESTCOAST_GATE, 1
	warp_event  2, 11, ROUTE2F_WESTCOAST_GATE, 2

	db 0 ; coord events
	;coord_event 31, 11, SCENE_R2F_MEET_SHIRENAI, R2FShirenai

	db 1 ; bg events
	bg_event 48, 8, BGEVENT_READ, Route2FGameHouseSign

	db 8 ; object events
	object_event 47,  8, SPRITE_COOLTRAINER_M, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, Route2FEldaryonScript, -1
	object_event 19, 11, SPRITE_FISHER, SPRITEMOVEDATA_SPINRANDOM_SLOW, 1, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, Route2FFisherScript, -1
	object_event  6,  4, SPRITE_FRUIT_TREE, SPRITEMOVEDATA_STILL, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, Route2FTree, -1
	object_event 31, 11, SPRITE_FISHER, SPRITEMOVEDATA_STANDING_UP, 1, 0, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_SCRIPT, 0, Route2FSuperNerdScript, EVENT_GAVE_MYSTERY_EGG_TO_ELM
	object_event 24,  2, SPRITE_YOUNGSTER, SPRITEMOVEDATA_STANDING_DOWN, 1, 0, -1, -1, PAL_NPC_GREEN, OBJECTTYPE_TRAINER, 3, TrainerYoungsterAlex, -1
	object_event 20, 11, SPRITE_YOUNGSTER, SPRITEMOVEDATA_STANDING_UP, 1, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_TRAINER, 5, TrainerPsychicEtika, -1
	object_event 31, 10, SPRITE_SILVER, SPRITEMOVEDATA_STANDING_UP, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, R2FShirenai, EVENT_ROUTE2F_UR_MOM_GAY
	object_event  7,  5, SPRITE_MONSTER, SPRITEMOVEDATA_STANDING_DOWN, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, R2FFangute, EVENT_ROUTE2F_SAW_FANGUTE