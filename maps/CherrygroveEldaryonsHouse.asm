	const_def 2 ; object constants
	const CHERRYGROVEELDARYONSHOUSE_COOLTRAINER_M
	const CHERRYGROVEELDARYONSHOUSE_COOLTRAINER_M2

CherrygroveEldaryonsHouse_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
CherrygroveEldaryonsHouseCredits:
	credits

CherrygroveEldaryonsHouseEldaryonScript:
	faceplayer
	opentext
	writetext CherrygroveEldaryonsHouseEldaryonText
	waitbutton
	checkflag EVENT_CHERRYGROVE_ELDARYON_WILL_HELP
	iftrue .EldaryonKnowsYou
	writetext EldaryonWishesBestText
	waitbutton
	closetext
	end

.EldaryonKnowsYou:
	writetext EldaryonWantsToHelpText
	yesorno
	iffalse .Refused
	writetext EldaryonGivesEevee1
	buttonsound
	waitsfx
	checkcode VAR_PARTYCOUNT
	ifequal PARTY_LENGTH, .NoRoom
	writetext EldaryonGivesEevee2
	playsound SFX_CAUGHT_MON
	waitsfx
	givepoke EEVEE, 5
	setevent EVENT_GOT_EEVEE
	writetext EldaryonWishesGoodLuck
	waitbutton
	clearflag EVENT_CHERRYGROVE_ELDARYON_WILL_HELP
	closetext
	end

.Refused:
	writetext EldaryonRefusal
	waitbutton
	closetext
	end

.NoRoom:
	writetext EldaryonNoRoom
	waitbutton
	closetext
	end

CherrygroveEldaryonsHouseBookshelf:
	jumpstd magazinebookshelf

EldaryonNoRoom:
	text "You gotta empty "
	line "some place before"
	cont "I can give you"

	para "this EEVEE."
	done

EldaryonWishesGoodLuck:
	text "Good luck on your"
	line "journey!"
	done

EldaryonGivesEevee2:
	text "<PLAYER> received"
	line "EEVEE!"
	done

EldaryonRefusal:
	text "Oh, don't worry."
	line "I'm just trying"
	cont "to help you."

	para "No tricks involved"
	line "!"
	done

EldaryonGivesEevee1:
	text "I hope you take"
	line "good care of it !"
	done

EldaryonWishesBestText:
	text "I hope you the"
	line "best for what's"

	para "next!"
	done

CherrygroveEldaryonsHouseEldaryonText:
	text "Hm hm!"
	line "The name's Eldaryon"

	para "and this is one of"
	line "my many homes."
	done

EldaryonWantsToHelpText:
	text "Oh. I do know you!"

	para "You're the newbie"
	line "from the HILLS!"

	para "I gotta help you"
	line "somehow!"

	para "I found this"
	line "#MON a while"
	cont "back."

	para "Would you like"
	line "it?"
	done

CherrygroveEldaryonsHouse_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  2,  7, CHERRYGROVE_CITY, 6
	warp_event  3,  7, CHERRYGROVE_CITY, 6

	db 0 ; coord events

	db 2 ; bg events
	bg_event  0,  1, BGEVENT_READ, CherrygroveEldaryonsHouseBookshelf
	bg_event  1,  1, BGEVENT_READ, CherrygroveEldaryonsHouseBookshelf

	db 2 ; object events
	object_event  2,  4, SPRITE_COOLTRAINER_M, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, CherrygroveEldaryonsHouseEldaryonScript, -1
	object_event  1,  2, SPRITE_COOLTRAINER_M, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, CherrygroveEldaryonsHouseCredits, -1
