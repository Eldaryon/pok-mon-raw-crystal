	const_def 2 ; object constants
	const BIRDWELL_CHILD

BirdwellTown_MapScripts:
	db 0 ; scene scripts

	db 1 ; callbacks
	callback MAPCALLBACK_NEWMAP, .Flypoint

.Flypoint:
	setflag ENGINE_FLYPOINT_BIRDWELL
	return

BirdwellTownYoungsterScript:
	jumptextfaceplayer BirdwellTownYoungsterText

BirdwellTownSign:
	jumptext BirdwellTownSignText

BirdwellEldaryonSign:
	jumptext BirdwellEldaryonSingText

BirdwellGymSign:
	jumptext BirdwellGymText

BirdwellTownYoungsterText:
	text "I hate the smell"
	line "of flowers!"
	done

BirdwellTownSignText:
	text "  Birdwell  Town"

	para "The Winds of"
	line "Dreams Flow in"
	cont "Our Grounds."
	done

BirdwellEldaryonSingText:
	text " Eldaryon's House"

	para "If it was a little"
	line "shinier, This"
	cont "panel would have"
	cont "been a mirror."
	done

BirdwellGymText:
	text "   Birdwell Gym"

	para "We are closed."
	line "Probably perman-"
	cont "ently."
	done


BirdwellTown_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  9,  19, FORSAKEN_FRONT_BIRDWELL_GATE, 1
	warp_event  10, 19, FORSAKEN_FRONT_BIRDWELL_GATE, 2

	db 0 ; coord events

	db 3 ; bg events
	bg_event 7,  11, BGEVENT_READ, BirdwellTownSign
	bg_event 3,  15, BGEVENT_READ, BirdwellEldaryonSign
	bg_event 16,  14, BGEVENT_READ, BirdwellGymSign

	db 1 ; object events
	object_event 3,  18, SPRITE_YOUNGSTER, SPRITEMOVEDATA_WALK_LEFT_RIGHT, 1, 0, -1, -1, PAL_NPC_BLUE, OBJECTTYPE_SCRIPT, 0, BirdwellTownYoungsterScript, -1
	