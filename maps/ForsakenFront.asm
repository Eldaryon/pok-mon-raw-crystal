	const_def 2 ; object constants

ForsakenFront_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks
ForsakenFrontSign:
	jumptext ForsakenFrontSignText

ForsakenFrontSignText:
	text "Forsaken Front"

	para "Stripped of life"
	line "by the Oblivion"
	cont "Wing."
	done

ForsakenFront_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  8,  45, FORSAKEN_FRONT_WEST_COAST_GATE, 1
	warp_event  9,  45, FORSAKEN_FRONT_WEST_COAST_GATE, 2
	warp_event  19,  3, FORSAKEN_FRONT_BIRDWELL_GATE, 3

	db 0 ; coord events

	db 1 ; bg events
	bg_event 6,  40, BGEVENT_READ, ForsakenFrontSign

	db 0 ; object events
