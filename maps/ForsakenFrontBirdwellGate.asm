	const_def 2 ; object constants

ForsakenFrontBirdwellGate_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

ForsakenFrontBirdwellGate_MapEvents:
	db 0, 0 ; filler

	db 4 ; warp events
	warp_event  4,  0, BIRDWELL_TOWN, 1
	warp_event  5,  0, BIRDWELL_TOWN, 2
	warp_event  4,  7, FORSAKEN_FRONT, 3
	warp_event  5,  7, FORSAKEN_FRONT, 3

	db 0 ; coord events

	db 0 ; bg events

	db 0 ; object events
