# Pokémon Crystal

This is the source code for Pokémon Raw Crystal.

It builds the ROM for the hack.
Thanks to everyone at pret for creating the disassembly.

To set up the repository, see [INSTALL.md](INSTALL.md). (It has been copied from the original pret project.)

**Discord:** [Click here to join.][discord]

**The original project is available** [here][pokecrystal].

[discord]: https://discord.gg/AkDHNwZ
[pokecrystal]: https://github.com/pret/pokecrystal
